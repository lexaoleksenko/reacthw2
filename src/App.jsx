import React from 'react'
import './reset.css'
import './App.scss';
import Modal from './components/modal/mod';
import Items from './components/item/items.jsx';
import Navbar from './components/navbar/navbar';

// Хотел реализовать созранение закрашивания иконок "избранного", отображения количества товаро в корзине и избранном.
// Но мозгов не хватило самому придумать а решение искать нет времени(
// По условию нужно просто добавить в localStorage , но если нужно реализовать полноценно то я доделаю)))


// let storSelItm = []
// document.addEventListener('DOMContentLoaded', function(){
//   for (let i = 0, len = 100; i < len; i++){
//     if(JSON.parse(localStorage.getItem(`selectItem${i}`)) !== null){
      
//       storSelItm.push(JSON.parse(localStorage.getItem(`selectItem${i}`)))
      
//     }
//   }
// });
// console.log(storSelItm)

// 
// 
// 


class App extends React.Component {
  constructor(props){
    super(props);
    this.state ={
      activeModal: false,
      items:
        // const fetchJson = async () => {
        //     try {
        //         const data = await fetch(url);
        //         const response = await data.json();
        //         console.log(response)  
        //     } catch (error) {
        //         console.log(error);
        //     }
        // };
        // fetchJson()
        //  должен был быть фетч
                [{
                    article: 1,
                    name: "Item 1",
                    price: "21$",
                    img: "./phph.jpg",
                    color: "col1",
                },
                {
                    article: 2,
                    name: "Item 2",
                    price: "22$",
                    img: "./phph.jpg",
                    color: "col2" ,
                },
                {
                    article: 3,
                    name: "Item 3",
                    price: "23$",
                    img: "./phph.jpg",
                    color: "col3",
                },
                {
                    article: 4,
                    name: "Item 4",
                    price: "24$",
                    img: "./phph.jpg",
                    color: "col4",
                },
                {
                    article: 5,
                    name: "Item 5",
                    price: "35$",
                    img: "./phph.jpg",
                    color: "col5",
                },
                {
                    article: 6,
                    name: "Item 6",
                    price: "36$",
                    img: "./phph.jpg",
                    color: "col6", 
                },
                {
                    article: 7,
                    name: "Item 7",
                    price: "37$",
                    img: "./phph.jpg",
                    color: "col7", 
                },
                {
                    article: 8,
                    name: "Item 8",
                    price: "48$",
                    img: "./phph.jpg",
                    color: "col8", 
                },
                {
                    article: 9,
                    name: "Item 9",
                    price: "49$",
                    img: "./phph.jpg",
                    color: "col9", 
                },
                {
                    article: 10,
                    name: "Item 10",
                    price: "52$",
                    img: "./phph.jpg",
                    color: "col10", 
                },
                {
                  article: 11,
                  name: "Item 11",
                  price: "21$",
                  img: "./phph.jpg",
                  color: "col1",
              },
              {
                  article: 12,
                  name: "Item 12",
                  price: "22$",
                  img: "./phph.jpg",
                  color: "col2" ,
              },
              {
                  article: 13,
                  name: "Item 13",
                  price: "23$",
                  img: "./phph.jpg",
                  color: "col3",
              },
              {
                  article: 14,
                  name: "Item 14",
                  price: "24$",
                  img: "./phph.jpg",
                  color: "col4",
              },
              {
                  article: 15,
                  name: "Item 15",
                  price: "35$",
                  img: "./phph.jpg",
                  color: "col5",
              },
              {
                  article: 16,
                  name: "Item 16",
                  price: "36$",
                  img: "./phph.jpg",
                  color: "col6", 
              },
              {
                  article: 17,
                  name: "Item 17",
                  price: "37$",
                  img: "./phph.jpg",
                  color: "col7", 
              },
              {
                  article: 18,
                  name: "Item 18",
                  price: "48$",
                  img: "./phph.jpg",
                  color: "col8", 
              },
              {
                  article: 19,
                  name: "Item 19",
                  price: "49$",
                  img: "./phph.jpg",
                  color: "col9", 
              },
              {
                  article: 20,
                  name: "Item 20",
                  price: "52$",
                  img: "./phph.jpg",
                  color: "col10", 
              },],
      cartItems: [

      ],
      cartItem: {},
      selectItems: [],
      
    }

    this.toggleModal = this.toggleModal.bind(this);
    this.addCart = this.addCart.bind(this);
    this.addSelect = this.addSelect.bind(this);
  }
  

  toggleModal(item) {
    const currentState = this.state.activeModal;
    this.setState({ activeModal: !currentState });
    this.setState({cartItem: item})
  };

  render(){
    return(
      <>
        <div className='shop'>
          <Navbar selLen={this.state.selectItems.length} cartLen={this.state.cartItems.length}/>
          <Modal styleMod={this.state.activeModal ? 'flex' : 'none' } title ="Add item to cart?" text = {`You sure? The price of this item: ${this.state.cartItem.price}`}  cls = {this.toggleModal} enter={this.addCart}/>
          <Items items={this.state.items} addItem={this.toggleModal} select = {this.addSelect}/>
        </div>
      </>
    )
  };

  addCart(){
    this.setState({cartItems: [...this.state.cartItems, this.state.cartItem]}, ()=>{
      this.setState({activeModal: false})
    })
  }

  addSelect(item){
    if(this.state.selectItems.includes(item)){
      for (let i = 0, len = this.state.selectItems.length; i < len; i++) {
        if (this.state.selectItems[i].article === item.article) {
          this.state.selectItems.splice(i, 1);
          this.setState({selectItems: [...this.state.selectItems]})
          localStorage.removeItem(`selectItem${item.article}`);
          break;
        }
      }
    } else {
      this.setState({selectItems: [...this.state.selectItems, item]}, ()=>{
      localStorage.setItem(`selectItem${item.article}`, JSON.stringify(item));
    })
    }
    
  }
}

export default App;
