import React from 'react';
import Cart from './cart';
import SelectItem from './selectItems';
import { faStore } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

class Navbar extends React.Component {
    
    constructor(props){
        super(props)
        
        this.state = {}
    }

    render() {
        return (
            <>
                <ul className='navbar'>
                    <li className="navbar__logo"><FontAwesomeIcon icon={faStore} className="navbar__logo_icon"  /><span className="navbar__logo_name">SuperShop</span></li>
                    <li className='navbar__select'><SelectItem selectLenght={this.props.selLen} starState={this.props.starState}/></li>
                    <li className='navbar__cart'><Cart cartLenght={this.props.cartLen}/></li>
                </ul>
            </>
        )
    }
}
    
export default Navbar