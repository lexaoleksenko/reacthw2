import React from 'react';
import { faCartShopping } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

class Cart extends React.Component {
    
    constructor(props){
        super(props)
        
        this.state = {}
    }

    render() {
        return (
            <>
            <FontAwesomeIcon icon={faCartShopping} className='navbar__cart_icon'/>
            <div className='navbar__cart_counter'>{this.props.cartLenght}</div>
            </>
        )
    }
}
    
export default Cart