import React from 'react';
import { faStar } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

class SelectItem extends React.Component {
    
    constructor(props){
        super(props)
        
        this.state = {}
    }

    render() {
        return (
            <>
                <FontAwesomeIcon icon={faStar} style={{color: this.props.starState}} className='navbar__select_icon'/>
                <div className='navbar__select_counter'>{this.props.selectLenght}</div>
            </>
        )
    }
}
    
export default SelectItem