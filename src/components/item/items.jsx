import React from 'react';
import Item from './item';



class Items extends React.Component {

    render() {
        return (
            <>
                <div className='items'>
                    {this.props.items.map((el) => (
                        <Item item={el} key={el.article} addItem={this.props.addItem} select={this.props.select} />
                    ))}
                </div>
                
            </>
        )
    }
}

export default Items