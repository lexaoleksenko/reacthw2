import React from 'react';
import { faStar } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

class Item extends React.Component {
    constructor(props){
        super(props);
        this.state ={
            starState: false
        }
        this.toggleStar = this.toggleStar.bind(this);
    }
    render() {
        return (
            <>
                <div className='items__item'>
                    <h2 className='items__item_name'>{this.props.item.name}</h2>
                    <img className='items__item_img' src={this.props.item.img} alt="#" />
                    <span className='items__item_price'>{this.props.item.price}</span>
                    <div>
                        <button className='items__item_btn' onClick={()=>{this.props.addItem(this.props.item)}}>Add to cart</button>
                        <FontAwesomeIcon className='items__item_star' icon={faStar} onClick={()=>{this.props.select(this.props.item); this.toggleStar()}} style={{color: this.state.starState ? "#FFFF00" : "#000000"}}/>
                    </div>
                    
                </div>
            </>
        )
    }
    toggleStar() {
        const currentState = this.state.starState;
        this.setState({ starState: !currentState });
    };
}
    
export default Item