import React from 'react';


class ButtonModCls extends React.Component {
    render() {
        return (
            <button onClick={this.props.onCloseClick}  className='btnMod' style={{backgroundColor: this.props.btnBgCol, color: this.props.btnCol}}>{this.props.btnTxt}</button>
        )
    }
}

export default ButtonModCls