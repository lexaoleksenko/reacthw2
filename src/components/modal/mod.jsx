import React from 'react';
import ButtonModCls from './buttonModCls';
import ButtonModEnt from './buttonModEnt';

import { faCircleXmark} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";


class Modal extends React.Component {
    constructor(props){
        super(props)
        
        this.state = {
            closeButton: true,
        }


    }

    render() {
        return (
            <div className='modal' style={{display: this.props.styleMod}}>
            <div className='modOver' onClick={this.props.cls}></div>
            <div className='modWind'>
                <div className='modHeader'>
                    <div className='headerTitle'>{this.props.title}</div>
                    <FontAwesomeIcon icon={faCircleXmark} className='headerClose' onClick={this.props.cls} style={{display: this.state.closeButton === true ? "block" : "none", color: "#000000" }}/>
                </div>
                <div className='modText'>{this.props.text}</div>
                <div className='modFooter'>
                    <ButtonModCls onCloseClick={this.props.cls} btnTxt="Cancel"/>
                    <ButtonModEnt onEnterClick={this.props.enter} btnTxt="Enter" btnBgCol="#696969" btnCol="#FFFFFF"/>
                </div>
            </div>
            </div>
            
            
        )
    }
}
    
    Modal.defaultProps = {
        title: "Modal title!",
        text: "22222222222",
        onSubmit: () => {},
    }

export default Modal