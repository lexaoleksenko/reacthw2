import React from 'react';


class ButtonModEnt extends React.Component {
    render() {
        return (
            <button onClick={this.props.onEnterClick}  className='btnMod' style={{backgroundColor: this.props.btnBgCol, color: this.props.btnCol}}>{this.props.btnTxt}</button>
        )
    }
}

export default ButtonModEnt